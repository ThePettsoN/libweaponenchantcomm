local AceComm = LibStub("AceComm-3.0")

local MAJOR, MINOR = "WeaponEnchantLib-1", 0
local WeaponEnchantLib = LibStub:NewLibrary(MAJOR, MINOR)

local WEAPON_ENCHANT_ADD_MSG_PREFIX = "WF_STATUS"
local WEAPON_ENCHANT_REMOVE_MSG_PREFIX = "WF_STATUS_REMOVE"

local WEAPON_ENCHANT_LOOKUP = {
	["124"] = 25557, -- Flametongue Totem Rank 1
	["285"] = 25557, -- Flametongue Totem Rank 2
	["543"] = 25557, -- Flametongue Totem Rank 3
	["1683"] = 25557, -- Flametongue Totem Rank 4
	["2637"] = 25557, -- Flametongue Totem Rank 5
	["1783"] = 25580, -- Windfury Totem Rank 1
	["563"] = 25580, -- Windfury Totem Rank 2
	["564"] = 25580, -- Windfury Totem Rank 3
	["2638"] = 25580, -- Windfury Totem Rank 4
	["2639"] = 25580, -- Windfury Totem Rank 5
}

local function havePartyMembers()
	return GetNumSubgroupMembers() > 0
end

function WeaponEnchantLib:RegisterInventoryEvent()
	if havePartyMembers() then
		if not self.frame:IsEventRegistered("UNIT_INVENTORY_CHANGED") then
			self.frame:RegisterUnitEvent("UNIT_INVENTORY_CHANGED", "player")
		end
	else
		self:Clean()
		self.frame:UnregisterEvent("UNIT_INVENTORY_CHANGED", "player")
	end
end

function WeaponEnchantLib:OnPlayerLogin(...)
	self.frame:RegisterEvent("GROUP_ROSTER_UPDATE")
	self:RegisterInventoryEvent()
end

function WeaponEnchantLib:OnGroupChange(...)
	self:RegisterInventoryEvent()
end

function WeaponEnchantLib:OnUnitInventoryChanged(...)
	C_Timer.After(0.1, function()
		-- Event triggers before new data is available
		-- By waiting 0.1 sec we allow new data to be available
		self:UpdateWeaponEnchant()
	end)
end

function WeaponEnchantLib:OnAddWeaponEnchant(prefix, msg)
	local guid, enchantId, expireTimestamp, lag = strsplit(';', msg)

        -- GUID
    local _, _, _, _, _, playerName, _ = GetPlayerInfoByGUID(guid)
    
    -- Convert spellId
    local spellId = WEAPON_ENCHANT_LOOKUP[enchantId]
    
    -- Expire
    expireTimestamp = tonumber(expireTimestamp)
    lag = tonumber(lag)
    local _, _, lagHome = GetNetStats()

    local expirationTime = GetTime() + (expireTimestamp - (lag + lagHome)) / 1000
    self.data[playerName] = {
    	spellId = spellId,
    	expirationTime = expirationTime,
    }
end

function WeaponEnchantLib:OnReamoveWeaponEnchant(prefix, data)
	local guid = data

    -- GUID
    local _, _, _, _, _, playerName, _ = GetPlayerInfoByGUID(guid)

	self.data[playerName] = nil    
end

function WeaponEnchantLib:UpdateWeaponEnchant()
	local _, expire, _, id, _, _, _, _ = GetWeaponEnchantInfo()
	if not id then
		if self.localData.id then
			local guid = UnitGUID("player")
			self.frame:SendCommMessage(WEAPON_ENCHANT_REMOVE_MSG_PREFIX, guid, "PARTY", "NORMAL")
			self.localData.id = 0
		end

		return -- No enchant active
	end

	local sId = tostring(id)
	if not WEAPON_ENCHANT_LOOKUP[sId] then
		return true-- Not a tracked enchant
	end

	local guid = UnitGUID("player")
	local _, _, lagHome = GetNetStats()
	local data = string.format("%s;%s;%d;%d", guid, sId, expire, lagHome)
	self.frame:SendCommMessage(WEAPON_ENCHANT_ADD_MSG_PREFIX, data, "PARTY", "NORMAL")
	self.localData.id = id
end

function WeaponEnchantLib:Clean()
	self.data = {}
	self.localData = {
		id = 0
	}
end

function WeaponEnchantLib:Initialize()
	local frame = CreateFrame("frame")
	AceComm:Embed(frame)

	frame:RegisterComm(WEAPON_ENCHANT_ADD_MSG_PREFIX, function(prefix, msg, ...)
		self:OnAddWeaponEnchant(prefix, msg, ...)
	end)
	frame:RegisterComm(WEAPON_ENCHANT_REMOVE_MSG_PREFIX, function(prefix, msg, ...)
		self:OnReamoveWeaponEnchant(prefix, msg, ...)
	end)

	frame:RegisterEvent("PLAYER_LOGIN")
	frame:SetScript("OnEvent", function(_, event, ...)
		if event == "PLAYER_LOGIN" then
			self:OnPlayerLogin(...)
		elseif event == "GROUP_ROSTER_UPDATE" then
			self:OnGroupChange(...)
		elseif event == "UNIT_INVENTORY_CHANGED" then
			self:OnUnitInventoryChanged()
		end
	end)

	self:Clean()

	self.frame = frame
	self.WEAPON_ENCHANT_LOOKUP = WEAPON_ENCHANT_LOOKUP

	self.initialized = true
end

function WeaponEnchantLib:GetPlayerData(playerName)
	return self.data[playerName]
end

local function main()
	WeaponEnchantLib:Initialize()
end

main()
