## Interface: 20501
## Title: LibWeaponEnchantComm
## Author: ThePettsoN
## Version: 1.0.2
## Notes: LibWeaponEnchantComm is a library that keeps track of Weapon Enchants provided by Shamans by both the player and the people they are grouped with.
## DefaultState: Enabled
## LoadOnDemand: 0

embeds.xml
main.lua
